# ## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.
class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_accessor :first_name, :last_name, :courses

  def name
    "#{first_name} #{last_name}"
  end

  def enroll(course)
    unless courses.include?(course)
      courses << course
      course.students << self
    end
    courses.each do |time|
      if time != course && time.conflicts_with?(course)
        raise 'Student already enrolled.'
      end
    end
  end

  def course_load
    depts_and_credits = Hash.new(0)
    courses.each do |course|
      depts_and_credits[course.department] += course.credits
    end
    depts_and_credits
  end
end
